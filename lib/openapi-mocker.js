const spawn = require('child_process').spawn;
const os = require('os');
const platform = os.platform();
const { resolve } = require('path');

module.exports = class OpenapiMocker {

    serve () {
        let path = this.getExecutabelPath();
        const inputFile = this.getInputFileAbsolutePath();
        console.log(path);

        spawn(path, ['serve', '--specification-url', inputFile], { stdio: 'inherit' });
    }

    getExecutabelPath() {
        let path = `${__dirname}/../bin/executables/`;
        if (this.runsOnOSX()) {
            path += 'openapi-mock-osx';
        }
        if (this.runsOnWindows()) {
            path += 'openapi-mock-windows.exe';
        }
        if (this.runsOnUnix()) {
            path += 'openapi-mock-unix';
        }
        return path;
    }

    runsOnOSX() {
        return platform === 'darwin';
    }

    runsOnWindows() {
        return platform === 'windows';
    }

    runsOnUnix() {
        return platform === 'freebsd' || platform === 'linux' || platform === 'openbsd';
    }

    getInputFileAbsolutePath() {
        return resolve(process.cwd(), process.argv[2]);
    }
}
